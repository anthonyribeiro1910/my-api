git checkout develop

git pull origin main

# Conferir quais são as tags já criadas e somar mais um
# Criar uma branch de release: release/v0.0.4

LAST_TAG=`git describe --abbrev=0 --tags`

TAGS=(${LAST_TAG//./ })

VNUM1=${TAGS[0]};
VNUM2=${TAGS[1]};
VNUM3=${TAGS[2]};
VNUM3=$((VNUM3+1));

NEW_TAG="$VNUM1.$VNUM2.$VNUM3";

git checkout -b release/$NEW_TAG;