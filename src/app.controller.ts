import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('bem-vindo')
  getHello2(): string {
    return 'Banco do Nordeste - Seja bem-vindo';
  }

  @Get('persons')
  listPersons(): string {

    const persons = ['Glaucio', 'Joao', 'Rafael', 'Banco do Nordeste'];

    return persons.join(' / ');

  }

  @Get('countries')
  listCountries(): string {

    const countries = ['Brasil', 'Argentina', 'Colômbia', 'Equador'];

    return countries.join(' / ');

  }

  @Get('my-route')
  showRoute(): string {

    const countries = ['Brasil', 'Argentina', 'Colômbia', 'Equador'];

    return countries.join(' / ');

  }
}
