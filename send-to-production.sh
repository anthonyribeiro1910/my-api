# Depois, criar uma tag que seja a mesma que o release e subir essa tag para o GitLab

LAST_TAG=`git describe --abbrev=0 --tags`

TAGS=(${LAST_TAG//./ })

VNUM1=${TAGS[0]};
VNUM2=${TAGS[1]};
VNUM3=${TAGS[2]};
VNUM3=$((VNUM3+1));

NEW_TAG="$VNUM1.$VNUM2.$VNUM3";

echo $NEW_TAG;

git tag -a $NEW_TAG -m "Release da tag $NEW_TAG";

git push --tags

BRANCH_NAME=`git rev-parse --abbrev-ref HEAD`;

echo $BRANCH_NAME;

# Subir as alterações da branch para criar um merge request

git push origin $BRANCH_NAME;